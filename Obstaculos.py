import pygame
import sys, random, math

pygame.init()

negro=pygame.Color('black')
rojo=pygame.Color('red')
blanco=pygame.Color('white')
gris=pygame.Color('gray')
colorFondo=blanco

witdh=900
height= 700
vel=0
salto=10
isJump=False
x=200
y=480

#Ventana y titulo

ventana = pygame.display.set_mode((witdh, height))
pygame.display.set_caption('Ladrillo')
clock=pygame.time.Clock()
#Fondo
ventana.fill(colorFondo)


#Bucle del juego

Terminar=False 
while not Terminar:
    for event in pygame.event.get():
        if event.type ==pygame.QUIT:
            Terminar = True
    if event.type ==pygame.KEYDOWN:

        if event.key ==pygame.K_LEFT: #Si es menor, no se desplazará
            vel= -50

        if event.key ==pygame.K_RIGHT: # Llega hasta el ancho menos el ancho del personaje
            vel= 50

     # Si no está en salto
        if event.key ==pygame.K_SPACE:
            isJump=True 

    if event.type ==pygame.KEYUP:

        if event.key ==pygame.K_LEFT: #Si es menor no voy a desplazar
            vel=0

        if event.key ==pygame.K_RIGHT: # Llega hasta el ancho menos el ancho del personaje
            vel=0

    if isJump: #si está en salto
        if salto >= -20: #caida hasta -20
            y -= (salto * abs(salto)) *0.5 #la variable va reduciendo hasta llegar arriba 10 y luego inverso hasta -20
            salto-=1 #Dec rementamos el valor de salto para obtener valores de y 
        
        else: 
            salto= 10 #reseteamos la variable para próximos saltos
            isJump=False #salgo del ciclo

#LÓGICA
    x += vel    #coordenadas x añaden velocidad

    if y>480:
        y=480

    if x<40:
        x=40
    if x>800:
        x=800


#Dibujo
    suelo=pygame.draw.rect(ventana, gris,(410,480,60,20))
    ladrillo=pygame.draw.rect(ventana,negro,(400,500,80,60))
    movil=pygame.draw.rect(ventana, rojo,(x,y,80,80))

#Colisión si viene de salto resetea valores y sale del bucle
    if movil.colliderect(ladrillo):
        if isJump:
            y-=20 
            salto=10 
            isJump=False

#Colisión lateral sin salto
    if vel ==50 and isJump ==False:
        x=x-50
    elif vel == -50 and isJump == False:
        x=x+50
    pygame.display.update()

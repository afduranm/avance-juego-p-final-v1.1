import pygame
from pygame.locals import *
import sys

pygame.init()

#Pantalla - Ventana 
W, H = 1000, 600 
PANTALLA = pygame.display.set_mode ((W, H))
pygame.display.set_caption("Personaje 1")
icono = pygame.image.load("cicla.png.png")
pygame.display.set_icon(icono)

#Fondo
fondo = pygame.image.load('fondo.png.png')

#Personaje

quieto = pygame.image.load ('cicla.png.png') 

caminaDerecha = [pygame.image.load ('cicla.png.png')]

caminaIzquierda = [pygame.image.load ('cicla.png.png')]

salta = [pygame.image.load('cicla.png.png')]

x=0
px=50
py=200
ancho= 40 
velocidad = 10

#Control de FPS

reloj = pygame.time.Clock()

#Variablessalto

salto= False

#Altura del salto

cuentaSalto= 10

#Variablesde dirección
izquierda = False
derecha = False

#Pasos
cuentaPasos=0

#Movimiento
def recargaPantalla():
    #variablesglobales
    global cuentaPasos
    global x 

    #fondo en movimiento 
    x_relativa = x % fondo.get_rect().width
    PANTALLA.blit(fondo) (x_relativa - fondo.get_rect().width, 0)
    if x_relativa < W:
        PANTALLA.blit(fondo, x_relativa, 0)
'''
    x-=5

    #contador de pasos 
    if cuentaPasos + 1 >= 1 :
        cuentaPasos=0
    #Movimiento a la izquierda
    if izquierda:
        PANTALLA.blit(caminaIzquierda[cuentaPasos // 1 ], (int(px), int(py)))
        cuentaPasos +=1

    #Movimiento a la derecha

    elif derecha:
        PANTALLA.blit(caminaDerecha[cuentaPasos // 1 ], (int(px), int(py)))
        cuentaPasos +=1
    #Salto
    elif salto + 1>=2:
        cuentaPasos=0
        PANTALLA.blit(salta[cuentaPasos // 1], (int(px), int(py)))
        cuentaPasos +=1
        
    else: PANTALLA.blit(quieto, (int(px), int(py)))

    #Actualizacion de la venta 
    pygame.display.update()'''

ejecuta= True 


#bucle de acciones y controles 
while ejecuta: 
    reloj.tick(18)

    #bucle del juego 
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            ejecuta = False 

#Opción tecla pulsada 
    keys = pygame.key.get_pressed()

    #Tecla A 
    if keys[pygame.K_a] and px > velocidad:
        px -= velocidad
        izquierda = True 
        derecha = False 

    #Tecla D 
    elif keys[pygame.K_d] and px < 900 - velocidad - ancho:
        px += velocidad
        izquierda = False 
        derecha = True
    #Quieto
    else:
        izquierda = False 
        derecha = False 
        cuentaPasos= 0 
    #Tecla W 
    if not (salto):
        if keys[pygame.K_w]:
            salto=True
            izquierda=False
            derecha=False
            cuentaPasos= 0

    else: 
        if cuentaSalto >= -10:
            py -= (cuentaSalto * abs(cuentaSalto)) * 0.5
            cuentaSalto -=1

        else: 
            cuentaSalto = 10 
            salto = False
    #Actualización de la ventana
    recargaPantalla()
#Salida de juego
pygame.quit()    



